// ==UserScript==
// @id             iitc-plugin-comm-filter-plus@ryokei
// @name           IITC plugin: COMM Filter Plus
// @author         ryokei
// @category       COMM
// @version        0.1.5.20220512.222650
// @namespace      https://github.com/iitc-project/ingress-intel-total-conversion
// @downloadURL    https://gitlab.com/ryokei-ingress/iitc/-/raw/master/iitc-comm-filter-plus/iitc-comm-filter-plus.user.js
// @description    Add "Dynamic Link" to [udnp-2018-11-06-195201] COMM Filter
// @include        https://*.ingress.com/intel*
// @include        http://*.ingress.com/intel*
// @match          https://*.ingress.com/intel*
// @match          http://*.ingress.com/intel*
// @include        https://*.ingress.com/mission/*
// @include        http://*.ingress.com/mission/*
// @match          https://*.ingress.com/mission/*
// @match          http://*.ingress.com/mission/*
// @include        https://intel.ingress.com/*
// @match          http://intel.ingress.com/*
// @grant          none
// ==/UserScript==
