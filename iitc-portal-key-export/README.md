# IITC Plugin: portal-key-export

Player Inventory というプラグインのポータルキーエクスポート機能に GUID も項目として追加しただけです。

## C.O.R.E サブスクリプション
インベントリー内のポータルキーの情報を取得する機能は、Ingress スキャナー内のストアから C.O.R.E サブスクリプション（月額600円前後？）をしていることが大前提となります。

必要となる期間中だけでもサブスクリプションしてください。

## インストール
お使いのIITCに下記URLからプラグインをインストールします。
https://gitlab.com/ryokei-ingress/iitc/-/raw/master/iitc-portal-key-export/iitc-portal-key-export.user.js

![Install on Android IITC-Mobile](assets/images/manual-01.jpeg)

## プラグイン有効化
プラグインのカテゴリーは **Keys** にしています。Keys カテゴリー内の当プラグインにチェックを入れて有効化します。
![Enable this plugin in the Keys category](assets/images/manual-02.jpeg)

※iOSの場合はチェックを入れた後にIITCのメイン画面でリロードする必要があるかもしれません。

## ポータルキー情報のエクスポート
IITCのInfoビュー（画面最下部タップで開く）から **Inventory Opt** を開きます。
![Open Inventory Opt](assets/images/manual-03.jpeg)

次に **Export Keys to Clipboard** ボタンを押します。
![Export Keys to Clipboard](assets/images/manual-04.jpeg)

Android の場合は、共有アクションから **Copy to Clipboard** を選択します。
![Copy to Clipboard on Android](assets/images/manual-05.jpeg)

iOS の場合は、共有アクションから **Copy** を選択します。
![Copy on iOS](assets/images/manual-06.jpeg)

## 共有
指定された方法で共有します。
スプレッドシートの場合は、新しいシートを作成してそこに上記の手順でコピーした内容を貼り付けしてシート名を「20230705 ryokei」のように共有した日と自分のAG名にしておくと分かりやすいかと思います。

## 注意事項
このプラグインはインベントリー内のすべてのポータルキーの情報を共有します。
不必要なポータルキーを省きたい場合、無関係なキーはすべてカプセルやキーロッカーにしまってからエクスポートを行い、スプレッドシートの場合、E列にカプセルのIDが入っている行をすべて削除してください。

以上