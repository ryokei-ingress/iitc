// ==UserScript==
// @name           Jack up mobileinfo
// @category       Misc
// @version        0.1.1
// @namespace      https://gitlab.com/ryokei-ingress/iitc
// @description    Jack up mobileinfo to avoid the conflict with iOS bottom control bar.
// @author         ryokei
// @updateURL      https://gitlab.com/ryokei-ingress/iitc/-/raw/master/iitc-jackup-mobileinfo/iitc-jackup-mobileinfo.meta.js
// @downloadURL    https://gitlab.com/ryokei-ingress/iitc/-/raw/master/iitc-jackup-mobileinfo/iitc-jackup-mobileinfo.user.js
// @match          https://intel.ingress.com/*
// @icon           https://www.google.com/s2/favicons?sz=64&domain=ingress.com
// @grant          none
// ==/UserScript==
