// ==UserScript==
// @name           Jack up mobileinfo
// @category       Misc
// @version        0.1.1
// @namespace      https://gitlab.com/ryokei-ingress/iitc
// @description    Jack up mobileinfo to avoid the conflict with iOS bottom control bar.
// @author         ryokei
// @updateURL      https://gitlab.com/ryokei-ingress/iitc/-/raw/master/iitc-jackup-mobileinfo/iitc-jackup-mobileinfo.meta.js
// @downloadURL    https://gitlab.com/ryokei-ingress/iitc/-/raw/master/iitc-jackup-mobileinfo/iitc-jackup-mobileinfo.user.js
// @match          https://intel.ingress.com/*
// @icon           https://www.google.com/s2/favicons?sz=64&domain=ingress.com
// @grant          none
// ==/UserScript==

function wrapper(plugin_info) {
// ensure plugin framework is there, even if iitc is not yet loaded
if(typeof window.plugin !== 'function') window.plugin = function() {};

//PLUGIN AUTHORS: writing a plugin outside of the IITC build environment? if so, delete these lines!!
//(leaving them in place might break the 'About IITC' page or break update checks)
plugin_info.buildName = 'release';
plugin_info.dateTimeVersion = '2022-08-04-203500';
plugin_info.pluginId = 'jackup-mobileinfo';
//END PLUGIN AUTHORS NOTE

// use own namespace for plugin
window.plugin.jackupMobileinfo = function() {};

window.plugin.jackupMobileinfo.MARGIN_BOTTOM = '3ex';

window.plugin.jackupMobileinfo.setupCSS = function() {
  $("<style>").prop("type", "text/css").html(''
  +'#mobileinfo{'
    +'margin-bottom:'+window.plugin.jackupMobileinfo.MARGIN_BOTTOM+';'
  +'}'
  ).appendTo("head");
}

var setup = function() {
  window.plugin.jackupMobileinfo.setupCSS();
}

setup.info = plugin_info; //add the script info data to the function as a property
if(!window.bootPlugins) window.bootPlugins = [];
window.bootPlugins.push(setup);
// if IITC has already booted, immediately run the 'setup' function
if(window.iitcLoaded && typeof setup === 'function') setup();
} // wrapper end
// inject code into site context
var script = document.createElement('script');
var info = {};
if (typeof GM_info !== 'undefined' && GM_info && GM_info.script) info.script = { version: GM_info.script.version, name: GM_info.script.name, description: GM_info.script.description };
script.appendChild(document.createTextNode('('+ wrapper +')('+JSON.stringify(info)+');'));
(document.body || document.head || document.documentElement).appendChild(script);
